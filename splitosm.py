#!/usr/bin/env python

HEAD="""<?xml version="1.0" encoding="UTF-8"?>
<osm version="0.6" generator="Overpass API 0.7.56.9 76e5016d">
<note>The data included in this document is from www.openstreetmap.org. The data is made available under ODbL.</note>
<meta osm_base="2021-09-27T07:54:58Z"/>
"""
TAIL="""
</osm>
"""

import sys
import os

from bs4 import BeautifulSoup

filename = sys.argv[1]
outdir = sys.argv[2]

with open(filename) as fp: 
    soup = BeautifulSoup(fp, features="lxml")


feature_count = len(soup.osm.findAll("relation"))

this = soup.osm.find("meta")
part = 1
while part <= feature_count:
    outputfilename = os.path.join(outdir, "output-"+str(part)+".osm")
    fd = open(outputfilename,"w")
    fd.write(HEAD)
    while True:
        this = this.next_sibling
        fd.write(str(this))
        if this.name == "relation":
            fd.write(TAIL)
            fd.close()
            break
    part = part + 1
    

