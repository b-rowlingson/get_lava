#!/bin/bash

now=`date +"%Y-%m-%d"`
./get_lava.py >volcano-${now}.osm

TEMP=`mktemp -d`

./splitosm.py volcano-${now}.osm $TEMP

for f in $TEMP/*.osm ; do ogr2ogr -oo CONFIG_FILE=osmconf.ini -append volcano-${now}.gpkg $f multipolygons; done
# clean up
rm -rf $TEMP


