#!/usr/bin/env python

Q="""
timeline(rel, 13249829);
foreach(
  retro(u(t["created"]))
  (
    (rel(13249829);>>;);
    out meta;
    
  );
);
"""

import requests
import json
overpass_url = "http://overpass-api.de/api/interpreter"
overpass_query = Q
response = requests.get(overpass_url, params={'data': overpass_query})

print(response.text)
