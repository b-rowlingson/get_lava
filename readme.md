# Reading Historical OSM Data Using Overpass API

## The 2021 Lava Flow from the Canary Islands

## Outline

* Get the OSM file from Overpass
* Split the OSM file into separate OSM files for each relation
* Convert and merge the OSM files into a GeoPackage

Note I'm doing this on a Linux box and using the shell throughout. 

## Get the OSM file

This script uses some Overpass wizardry 
with great thanks to [mmd](https://gis.stackexchange.com/users/37540/mmd) 
on GIS Stack Exchange. I [asked](https://gis.stackexchange.com/questions/412365/getting-osm-features-by-id-at-a-version-number) and got a reply.

The python code only uses `requests` and `json` which should be in your standard python library.
The OSM ID of the lava flow is hard-coded into the script. The standard output is an XML file in OSM format
which I'm sending here to a file of the form `volcano-2021-10-01.osm`:

```
now=`date +"%Y-%m-%d"`
./get_lava.py >volcano-${now}.osm
```

## Split the OSM file

You'd think the next step would be running `ogr2ogr` to convert the OSM to GeoPackage. This 
doesn't work. Each relation in the OSM file is related to the nodes and ways defined before it 
in the XML, and I think the OGR driver for OSM doesn't handle this well. A simple conversion 
results in one feature and I've tried some of the driver options to no avail.

Instead this python script steps through the XML, writing all the node and way info until it finds
a relation, then writes that, closes the XML, and starts writing to a new file. It repeats until
it has written a file for each relation in the OSM file.

You will need to install the BeautifulSoup html/xml parser for this. Using `pip`, that's `python -m
pip install BeautifulSoup`, or use your package manager, or `conda`, or rewrite this to use whatever
XML python you prefer. The script takes a filename of the OSM data and a folder to stick the 
parts. This is temporary, you won't need it eventually:

```
rm -rf tmp
mkdir tmp
./splitosm.py volcano-${now}.osm ./tmp/
```

## Convert and Merge

Next we can run `ogr2ogr` and merge all the OSM parts into one geopackage. We use a custom `osmconf.ini` file
to get the timestamp from the `relation` tag, and we only select the multipolygons from the OSM - there shouldn't
be anything else useful in there:

```
for f in tmp/*.osm ; do ogr2ogr -oo CONFIG_FILE=osmconf.ini -append volcano-${now}.gpkg $f multipolygons; done
# clean up
rm -rf tmp
```

## Conclusion

That's it - you have a GeoPackage which you can load into QGIS or whereever and style and even use the temporal tools
on. Each feature should have a timestamp.

Oddly some parts seem to have more than one multipolygon, which I'm not sure I understand but maybe I'll investigate later.
Note that since this is every saved OSM version there may be errors - don't believe this is truth. You might also want
to look at the [Copernicus](https://emergency.copernicus.eu/mapping/list-of-components/EMSR546) emergency management
service data for this incident. For now I've got some time-series polygons that might be interesting for student
projects.

## Further Work

If you want you could make this more flexible to enable extraction of any feature with history - currently
the OSM_ID is hard coded into the script. You could also automate the whole process in Python instead of having
the separate steps. You might want to extract more than just the multipolygons.

Feel free to add comments and criticisms to the issue tracker.

## License

This is all MIT-Licensed (Terms and Conditions in the LICENSE file)
code and documentation, so go forth and play with it. Any OSM data you
download has its own licensing and usage terms, so respect that.


